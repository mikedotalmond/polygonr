package polygonr.view;

import flash.display.Bitmap;

import flash.events.KeyboardEvent;

import flash.ui.Keyboard;

import haxe.ui.toolkit.containers.ScrollView;

import haxe.ui.toolkit.controls.Image;
import haxe.ui.toolkit.controls.Slider;
import haxe.ui.toolkit.controls.Text;

import haxe.ui.toolkit.core.base.HorizontalAlign;

import haxe.ui.toolkit.core.interfaces.IDisplayObject;

import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.XMLController;

import haxe.ui.toolkit.data.DataManager;
import haxe.ui.toolkit.data.FilesDataSource;

import haxe.ui.toolkit.events.UIEvent;

import haxe.ui.toolkit.themes.GradientTheme;

import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;

import openfl.events.Event;

import openfl.Lib;

import openfl.net.URLRequest;

import polygonr.view.UI.UIController;

import systools.Dialogs;

/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 */
class UI {
	
	static public var controller(default, null):UIController;
	
	public static function init():UIController {
		
		Toolkit.defaultTransition = "none";
		Toolkit.theme = new GradientTheme();
		
		Toolkit.init();
		
		controller = new UIController();
		
		Toolkit.openFullscreen(function(root) {
			root.addChild(controller.view);
		});
		
		return controller;
	}
}


@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/ui.xml"))
class UIController extends XMLController {
	
	public var filesDataSource		(default, null)	:FilesDataSource;
	
	public var sourceBitmap							:Bitmap;
	
	public var modeSelect			(default, null)	:Signal1<Int>;
	public var decompositionSelect	(default, null)	:Signal1<String>;
	public var copyToClipboard		(default, null)	:Signal0;
	public var saveFile				(default, null)	:Signal1<String>;
	public var imageSelected		(default, null)	:Signal2<String, Bitmap->Void>;
	public var spritesheetSelected	(default, null)	:Signal2<String, Bitmap->Void>;
	public var tabChange			(default, null)	:Signal1<Int>;
	public var sliderChange			(default, null)	:Signal2<String, Float>;
	
	public function new() {
		
		copyToClipboard		= new Signal0();
		saveFile			= new Signal1<String>();
		decompositionSelect	= new Signal1<String>();
		imageSelected		= new Signal2<String, Bitmap->Void>();
		spritesheetSelected	= new Signal2<String, Bitmap->Void>();
		tabChange			= new Signal1<Int>();
		modeSelect			= new Signal1<Int>();
		sliderChange		= new Signal2<String, Float>();
		
		filesDataSource 	= cast DataManager.instance.getRegisteredDataSource('filesData');
		filePath.text 		= filesDataSource.dir;
		filePath.addEventListener(KeyboardEvent.KEY_DOWN, fileInputKeyDownHandler);
		
		attachEvent('mainTabs', UIEvent.CHANGE, tabChangeHandler);
		attachEvent('fileList', UIEvent.CHANGE, fileSelectHandler);
		attachEvent('browseButton', UIEvent.CLICK, browseHandler);
		attachEvent('imageModeButton', UIEvent.CLICK, modeSelectHandler.bind(_, Main.MODE_IMAGE));
		attachEvent('spritesheetModeButton', UIEvent.CLICK, modeSelectHandler.bind(_, Main.MODE_SPRITESHEET));
		attachEvent('decompositionSelector', UIEvent.CHANGE, onDecompositionSelect);
		attachEvent('btn_save', UIEvent.CLICK, saveHandler);
		attachEvent('btn_copyToClipboard', UIEvent.CLICK, copyToClipboardHandler);
		attachEvent('sourceCode', UIEvent.CLICK, sourcecodeButtonHandler);
		attachEvent('twitter', UIEvent.CLICK, twitterButtonHandler);
		
		attachEvent('previewScrollview', UIEvent.RESIZE, scrollerHandler.bind(_, previewImage));
		attachEvent('processScrollview', UIEvent.RESIZE, scrollerHandler.bind(_, processImage));
		
		var sliderIds = ['alphaThreshold', 'granularityX', 'granularityY', 'quality', 'simplification'];
		for (id in sliderIds) {
			var slider:Slider = cast getComponent(id);
			var label:Text = cast getComponent('${id}_label');
			slider.addEventListener(UIEvent.CHANGE, function(_) {
				if (id == 'simplification') label.text = '${slider.pos/10}';
				else label.text = '${slider.pos}';
				sliderChange.dispatch(id, slider.pos);
			});
		}
	}
	
	
	function modeSelectHandler(_, flag) modeSelect.dispatch(flag);		
	function onDecompositionSelect(_) decompositionSelect.dispatch(decompositionSelector.text);
	
	function copyToClipboardHandler(_) copyToClipboard.dispatch();	
	function twitterButtonHandler(_) Lib.getURL(new URLRequest('https://twitter.com/mikedotalmond'));	
	function sourcecodeButtonHandler(_) Lib.getURL(new URLRequest('https://bitbucket.org/mikedotalmond/polygonr'));
	
	
	function scrollerHandler(e:Event, content:IDisplayObject) {
		content.horizontalAlign = HorizontalAlign.CENTER;
		if (content.height < e.target.height) {
			content.y = content.parent.height / 2 - content.height / 2;
		}
	}	
	
	
	function saveHandler(_) {
		var filename = Dialogs.saveFile("Save file", "", "");
		if (filename != null && filename.length > 0) {
			if (filename.indexOf(".json") == -1) filename = filename + '.json';
			saveFile.dispatch(filename);
		}
	}
	
	
	function browseHandler(_) {
		var path = Dialogs.folder('Select a Folder...', "");
		if (path != null && path.length > 0) setNewPath(path);
	}
	
	
	function fileInputKeyDownHandler(e:KeyboardEvent) {
		if (e.keyCode == Keyboard.ENTER) {
			if (filePath.text != filesDataSource.dir) {
				setNewPath(filePath.text);
			}
		}
	}
	
	
	function setNewPath(filePath:String) {
		var oldDir = filesDataSource.dir;
		filesDataSource.removeAll();
		filesDataSource.createFromString(filePath);
		if (!filesDataSource.open()) {
			if (oldDir != null) {
				filesDataSource.createFromString(oldDir);
				filesDataSource.open();
			}
		}
	}
	
	
	function fileSelectHandler(_:Event) {
		
		if (fileList.selectedItems.length == 1) {
			
			var firstItem = fileList.selectedItems[0];
			var data = firstItem.data;
			
			if (data.isParent) {
				filesDataSource.openParent();
			} else if (data.isDir) {
				filesDataSource.openSubdirectory(data.text);
			} else if (data.isFile) {
				fileSelected('${filesDataSource.dir}/${data.text}');
			}
			
			filePath.text = filesDataSource.dir;
		}
	}
	
	
	function fileSelected(filePath:String) {
		//trace('file:///${filePath}');
		var filePathL = filePath.toLowerCase();
		if (filePathL.indexOf('.png') != -1) {
			imageSelected.dispatch(filePath, previewLoadHandler);
		} else if (filePathL.indexOf('.xml') != -1 || filePathL.indexOf('.json') != -1) {
			spritesheetSelected.dispatch(filePath, previewLoadHandler);
		}
	}
	
	
	function tabChangeHandler(_) {
		if (mainTabs.selectedIndex == 1) {
			setImageSource(processImage, processScrollview);
		} else {
			setImageSource(previewImage, previewScrollview);
		}
		tabChange.dispatch(mainTabs.selectedIndex);
	}
	
	
	function previewLoadHandler(b:Bitmap) {
		if (sourceBitmap != null) {
			sourceBitmap.bitmapData.dispose();
			sourceBitmap = null;
		}
		sourceBitmap = b;		
		setImageSource(previewImage, previewScrollview);
	}
	
	
	function setImageSource(image:Image, scroller:ScrollView) {
		if (sourceBitmap != null) {
			image.sprite.removeChildren();
			image.sprite.addChild(sourceBitmap);
			image.width = sourceBitmap.width;
			image.height = sourceBitmap.height;
			
			image.horizontalAlign = HorizontalAlign.CENTER;	
			image.y = scroller.height / 2 - sourceBitmap.height / 2;
		}
	}
}