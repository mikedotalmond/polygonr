package polygonr.model;

import flash.display.BitmapData;
import flash.display.Graphics;
import nape.geom.AABB;
import nape.geom.Vec2;
import nape.phys.Body;
import polygonr.model.IsoBody.DecompositionMode;

import polygonr.model.SpriteSheet.StarlingSpriteSheet;


/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 */
class BitmapToBody {
	
	
	public static function decomposeSpritesheet(map:Map<String,BitmapData>, alphaThreshold = 0x80, granularity:Vec2, quality:Int = 2, simplification:Float = 4.0, mode:DecompositionMode=null):Array<Body> {
		
		var out = [];
		var it:Iterator<String> = map.keys();
		for (key in it) {
			var bitmap = map.get(key);
			var b:Body = decompose(bitmap, null, alphaThreshold, granularity, quality, simplification, mode);
			b.userData.serialisedName = key;
			out.push(b);
		}
		
		return out;
	}
	
	
	public static function decompose(data:BitmapData, bounds:AABB=null, alphaThreshold = 0x80, granularity:Vec2, quality:Int = 2, simplification:Float = 4.0, mode:DecompositionMode=null):Body {
		
		data.lock();
		
		var iso		= new BitmapDataIsoFunction(data, alphaThreshold, bounds);
		var b 		= iso.bounds;
		
		/** @see: http://napephys.com/help/manual.html ~~ 9. Geometric Utilities */
		var body:Body = IsoBody.run(iso.iso, b, granularity, quality, simplification, mode);
		
		SerialiseBodyUtil.serialiseBody(body);
		
		return body;
	}
	
	
	/**
	 * 
	 * @param	body
	 * @param	graphics
	 */
	public static function drawBodyPolys(body:Body, graphics:Graphics, ?offset:Vec2=null) {
		
		var graphicOffset:Vec2 = body.userData.graphicOffset;
		if (graphicOffset == null) graphicOffset = Vec2.get();
		if (offset == null) offset = Vec2.get();
		
		for (shape in body.shapes) {
		
			var poly 	= shape.castPolygon;
			var v0 		= poly.localVerts.at(0).add(offset).sub(graphicOffset);
			
			graphics.moveTo(v0.x, v0.y);
			graphics.lineStyle(1, 0xff0000);
			graphics.beginFill(0,.5);
			
			for (lVert in poly.localVerts) {
				var v = lVert.add(offset).sub(graphicOffset);
				graphics.lineTo(v.x, v.y);
			}
			
			graphics.lineTo(v0.x, v0.y);
			graphics.endFill();
		}
	}
}