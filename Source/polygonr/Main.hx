package polygonr;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.Shape;
import haxe.ui.toolkit.controls.selection.ListSelector;
import openfl.display.Sprite;
import polygonr.model.IsoBody.DecompositionMode;

import flash.events.Event;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;

import haxe.Timer;

import haxe.ui.toolkit.controls.Slider;
import haxe.ui.toolkit.core.PopupManager;

import msignal.Signal.Signal1;

import nape.geom.Vec2;
import nape.phys.Body;
import nape.space.Space;

import polygonr.model.BitmapToBody;
import polygonr.model.SerialiseBodyUtil;

import polygonr.model.SpriteSheet.JSSpriteSheet;
import polygonr.model.SpriteSheet.StarlingSpriteSheet;
import polygonr.model.SpriteSheet.SubTexture;

import polygonr.view.UI;
import polygonr.view.UI.UIController;

import sys.io.File;
import systools.Clipboard;


/**
 */
class Main extends Sprite {

	public static inline var MODE_IMAGE			:Int = PopupButton.CUSTOM;
	public static inline var MODE_SPRITESHEET	:Int = PopupButton.CUSTOM + 1;

	var uiController		:UIController;

	var sourceBitmapData	:BitmapData;
	var sourceBitmapDisplay	:Bitmap;

	var space				:Space;
	var bodyDebugShape		:Shape;
	var updateTimeout		:Timer;
	var body				:Body;
	var bitmapLoaded		:Signal1<Bitmap>;
	var spriteSheetMap		:Map<String, BitmapData>;
	var bodies				:Array<Body>;
	var mode				:Int;
	var decomposeMode		:DecompositionMode;
	
	public function new () {

		super();

		bodyDebugShape 	= new Shape();
		uiController 	= UI.init();

		decomposeMode	= DecompositionMode.Convex;
		bitmapLoaded	= new Signal1<Bitmap>();

		uiController.tabChange.add(onTabChange);
		uiController.sliderChange.add(onSliderChange);
		uiController.imageSelected.add(loadBitmap);
		uiController.spritesheetSelected.add(onSpriteSheetSelected);
		uiController.saveFile.add(onSave);
		uiController.copyToClipboard.add(onCopyToClipboard);
		uiController.decompositionSelect.add(decompositionModeSelected);
		uiController.modeSelect.add(modeSelect);
		
		PopupManager.instance.showSimple("Pick operation mode", "Mode Select", [
			{ type:MODE_IMAGE, text:'Image (png)' },
			{ type:MODE_SPRITESHEET, text:'SpriteSheet (xml,json)' }
		], modeSelect);
	}
	

	function modeSelect(flag:Int) {

		switch (flag) {

			case MODE_IMAGE:
				uiController.filesDataSource.fileFilter	= ~/.(png)$/i;
				mode = flag;
			case MODE_SPRITESHEET:
				uiController.filesDataSource.fileFilter	= ~/.(xml|json)$/i;
				mode = flag;
			default:
				throw "Bad flag!";
		}

		uiController.filesDataSource.update(null);
		
		if (bodies != null && bodies.length > 0) {
			for (body in bodies) {
				body.space = null;
				body = null;
			}
			bodies = null;
		}
		
		if (spriteSheetMap != null) {
			for (item in spriteSheetMap) {
				item.dispose();
				item = null;
			}
			spriteSheetMap = null;
		}
		
		
		uiController.previewImage.sprite.removeChildren();
		uiController.processImage.sprite.removeChildren();
		if (uiController.sourceBitmap != null) {
			uiController.sourceBitmap.bitmapData.dispose();
			uiController.sourceBitmap.bitmapData = null;
			uiController.sourceBitmap = null;
		}
	}

	
	function decompositionModeSelected(name:String) {
		var mode = DecompositionMode.createByName(name);
		if (mode == null) mode = DecompositionMode.Convex;
		
		if (mode != decomposeMode) {
			decomposeMode = mode; 
			process();
		}
	}
	

	function onSpriteSheetSelected(path:String, imageLoadedCallback:Bitmap->Void) {
		var data = File.getContent(path);
		if (data != null) {
			var lastDot 	= path.lastIndexOf('.');
			var lastSlash 	= path.lastIndexOf('/');
			var extension 	= path.substring(lastDot + 1);
			var fileName	= path.substring(lastSlash + 1, lastDot);
			var filePath	= path.substring(0, lastSlash);
			
			body = null;
			spriteSheetMap = null;
			bitmapLoaded.addOnce(function(b) {
				// setup spritesheet
				if (extension == 'xml') {
					spriteSheetMap = StarlingSpriteSheet.parse(data, b.bitmapData);
				} else if (extension == 'json') {
					spriteSheetMap = JSSpriteSheet.parse(data, b.bitmapData);
				}
			});
			
			if (fileName == 'library' && extension == 'json') {
				// flump - can have multiple texture bitmap
				var files = JSSpriteSheet.getLibraryAssetsFileNames(data);
				trace(files);
				loadBitmap('${filePath}/${files[0]}', imageLoadedCallback);
			} else {
				loadBitmap('${filePath}/${fileName}.png', imageLoadedCallback);
			}
			
		} else {
			trace('Error loading spritesheet file at: ${path}');
		}
	}
	
	
	function loadBitmap(path:String, completeHandler:Bitmap->Void) {
		var loader = new Loader();
		bitmapLoaded.addOnce(completeHandler);
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, bitmapLoadHandler);
		loader.load(new URLRequest(path), new LoaderContext(false, ApplicationDomain.currentDomain));
	}


	function bitmapLoadHandler(e:Event):Void {
		var loader:LoaderInfo = e.target;
		loader.removeEventListener(Event.COMPLETE, bitmapLoadHandler);
		bitmapLoaded.dispatch(cast(loader.content));
	}


	function onCopyToClipboard() {
		switch(mode) {
			case MODE_IMAGE:
			if (body != null && body.userData.serialised != null) {
				Clipboard.setText(body.userData.serialised);
				PopupManager.instance.showSimple("Data copied to clipboard", "Success!");
			}

			case MODE_SPRITESHEET:
			if (bodies != null && bodies.length > 0) {
				Clipboard.setText(SerialiseBodyUtil.serialiseBodies(bodies));
				PopupManager.instance.showSimple("Data copied to clipboard", "Success!");
			}
		}
	}
	

	function onSave(path:String) {
		var data = null;

		if (mode == MODE_IMAGE && body != null && body.userData.serialised != null) {
			data = body.userData.serialised;
		} else if (mode == MODE_SPRITESHEET && bodies != null && bodies.length > 0) {
			data = SerialiseBodyUtil.serialiseBodies(bodies);
		}

		if (data != null) {
			writeTextFile(path, data);
			PopupManager.instance.showSimple("File saved", "Success!");
		}
	}
	

	function onSliderChange(id:String, val:Float) {
		if (uiController.sourceBitmap != null) {
			if (updateTimeout != null) updateTimeout.stop();
			updateTimeout = Timer.delay(process, 333);
		}
	}

	
	function onTabChange(index:Int) {
		if (index == 1) {
			if(uiController.sourceBitmap != null) process();
		}
	}

	
	function process():Void {
		
		var cnt	= uiController;
		var b 	= cnt.sourceBitmap;
		var bd 	= b.bitmapData;

		var alphaThreshold	:Int 	= Std.int(cnt.alphaThreshold.pos);
		var quality			:Int 	= Std.int(cnt.quality.pos);
		var simplification	:Float	= cnt.simplification.pos / 10;
		var granularity		:Vec2 	= Vec2.get(cnt.granularityX.pos, cnt.granularityY.pos);
		
		// process view...
		// TODO: make min poly area configurable - currently fixed to 1.0 in IsoBody.run
		// TODO: option/slider to adjust the polygon overlay alpha
		// TODO: add info/readout for number of polys, edges, vertices
		
		bodyDebugShape.graphics.clear();
		if (bodyDebugShape.parent == null) cnt.processImage.sprite.addChild(bodyDebugShape);
		
		body = null; bodies = null;
		if (mode == MODE_IMAGE) {
			body = BitmapToBody.decompose(bd, null, alphaThreshold, granularity, quality, simplification, decomposeMode);
			BitmapToBody.drawBodyPolys(body, bodyDebugShape.graphics);
		} else {
			bodies = BitmapToBody.decomposeSpritesheet(spriteSheetMap, alphaThreshold, granularity, quality, simplification, decomposeMode);
			for (b in bodies) {
				var name	:String = b.userData.serialisedName;
				var texture	:SubTexture = cast spriteSheetMap.get(name);
				BitmapToBody.drawBodyPolys(b, bodyDebugShape.graphics, Vec2.get(texture.x, texture.y));
			}
		}
	}




	/**
	 * @param	name
	 * @param	content
	 */
	static function writeTextFile(name:String, content:String) {
		var fout = File.write(name, false);
		fout.writeString(content);
		fout.close();
	}
}